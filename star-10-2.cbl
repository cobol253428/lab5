       IDENTIFICATION  DIVISION. 
       PROGRAM-ID. STAR-10-2.
       AUTHOR. FHANGLERR

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  QTY    pic 9(2) VALUE ZERO.

       PROCEDURE DIVISION.
       000-Begin.
           PERFORM 001-PRINT-STAR-INLINE THRU 001-EXIT 
           GOBACK
       .

       001-PRINT-STAR-INLINE.
           PERFORM 002-PRINT-ONE-STAR WITH TEST BEFORE UNTIL QTY = 10
           DISPLAY ""
       .

       001-EXIT.
           EXIT
       .

       002-PRINT-ONE-STAR.
           DISPLAY "*" WITH NO ADVANCING 
           COMPUTE QTY = QTY + 1
       .