       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRIANGLE1.
       AUTHOR. FHANGLERR.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SCR-LINE           PIC X(80)   VALUE SPACES.
       01 STAR-NUM           PIC 99      VALUE ZEROS.
          88 VALID-STAR-NUM              VALUE 1 THRU 80.
       01 INDEX-NUM          PIC 99      VALUE ZEROS.

       PROCEDURE DIVISION .
       000-Begin.
           PERFORM 002-INPUT-STAR-NUM THRU 002-EXIT 
           PERFORM 001-PRIN-STAR-LINE THRU 001-EXIT
              VARYING INDEX-NUM FROM 1 BY 1 UNTIL INDEX-NUM > STAR-NUM
           GOBACK
       .

       001-PRIN-STAR-LINE.
           MOVE ALL "*" TO SCR-LINE(1:INDEX-NUM)
           DISPLAY SCR-LINE
       .

       001-EXIT.
           EXIT
       .

       002-INPUT-STAR-NUM.
           PERFORM UNTIL VALID-STAR-NUM 
              DISPLAY "Please input star number : " WITH NO ADVANCING
                 ACCEPT STAR-NUM
                 IF NOT STAR-NUM = 0
                    DISPLAY "Please input star num in positive number"
           END-PERFORM
       .
           
       002-EXIT.
           EXIT
       .
